var phone;
var RIDES = {
	property: 10,

	initialize: function() {
		RIDES.userData();
		RIDES.loadData();
	},
	userData: function(){
		// Carga el usuario y el numero de telefono.
		// para mostrar el nombre de usuario y el telefono para validar los rides.
		var user = [];
		user = JSON.parse(localStorage.getItem('user-log'));
		var object = user[0]
		phone = user[1];
		document.getElementById("user-name").innerHTML = object;
	},

	loadData: function(){
		debugger;
		var ride = [JSON.parse(localStorage.getItem('ridesEdit'))];

		for (var i = ride.length - 1; i >= 0; i--) {
			var objectRide = ride[i];
			//Setear toda la informacion del ride seleccionado.
			document.getElementById("rideName").value = objectRide.rideName;
			document.getElementById("startFrom").value = objectRide.startFrom;
			document.getElementById("end").value = objectRide.end;
			document.getElementById("description").value = objectRide.description;
			document.getElementById("departure").value = objectRide.departure;
			document.getElementById("arrival").value = objectRide.arrival;

			//Dias de la semana.
			for (var i=0; objectRide.days[i]; ++i) {
				var day = objectRide.days[i];
				switch (day) {
					case "Monday":
					document.getElementById("Monday").checked = true;
					break;
					case "Tuesday":
					document.getElementById("Tuesday").checked = true;
					break;
					case "Wednesday":
					document.getElementById("Wednesday").checked = true;
					break;
					case "Thursday":
					document.getElementById("Thursday").checked = true;
					break;
					case "Friday":
					document.getElementById("Friday").checked = true;
					break;
					case "Saturday":
					document.getElementById("Saturday").checked = true;
					break;
					case "Sunday":
					document.getElementById("Sunday").checked = true;
					break;
				}
			}
		}
	},
};

function rideEditSave(){
	debugger;
	var rideName = document.getElementById('rideName').value;
	var startFrom = document.getElementById('startFrom').value;
	var end = document.getElementById('end').value;
	var description = document.getElementById('description').value;
	var departure = document.getElementById('departure').value;
	var arrival = document.getElementById('arrival').value;
	//Almacenar dias de la semana a un array.
	var days = [];
	var inputElements = document.getElementsByName('days');
	for(var i=0; inputElements[i]; ++i){
		if(inputElements[i].checked){
			days.push(inputElements[i].value);
		}
	}

		//Se crea el objeto.
		var editRide = {
			rideName,
			startFrom,
			end,
			description,
			departure,
			arrival,
			days,
			phone
		};
		var position = JSON.parse(localStorage.getItem('position'));
		//Agrega el ride editado.
		var ridesArrayBackup = [];
		var ridesArray = [JSON.parse(localStorage.getItem('rides'))];
		for (var i = ridesArray.length - 1; i >= 0; i--) {
			array = ridesArray[i];
			for (var i = array.length - 1; i >= 0; i--) {
				if (position == i) {
					//Agregar el ride en la posicion que estaba anteriormente.
					ridesArrayBackup[position] = editRide;
					alert("Ride Editado con exito");
				}else{
					var object =  array[i];
					ridesArrayBackup[i] = object;
				}
			}
		}
		//Guardarlo en el localStorage.
		localStorage["rides"] = JSON.stringify(ridesArrayBackup);
		//Limpiar los inputs.
		document.getElementById('rideName').value = "";
		document.getElementById('startFrom').value = "";
		document.getElementById('end').value = "";
		document.getElementById('description').value = "";
		document.getElementById('departure').value = "";
		document.getElementById('arrival').value = "";
	}

	RIDES.initialize();
