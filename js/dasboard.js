var phone;
var RIDES = {
	property: 10,

	initialize: function() {
		RIDES.userData();
		RIDES.loadUsers();

	},

	userData: function(){
		// Carga el usuario y el numero de telefono.
		// para mostrar el nombre de usuario y el telefono para validar los rides.
		var user = [];
		user = JSON.parse(localStorage.getItem('user-log'));
		var object = user[0];
		phone = user[1];
		document.getElementById("user-name").innerHTML = object;

	},
	loadUsers: function() {
		debugger;
      //leer de localStorage los usuarios
      var rides = [];
      if (localStorage.getItem('rides')) {
      	rides = JSON.parse(localStorage.getItem('rides'));
      }
      //agregar cada usuario al DOM
      rides.forEach(function(rides, index, users) {
      	if (rides.phone == phone) {
      		RIDES.addUser(rides);
      	}
      });

  },
  addUser: function(rides) {
	  debugger;
        // crear una HTML fila
        var row = "<tr><td>"+rides.rideName+"</td><td>"+rides.startFrom+"</td><td>"+rides.end+"</td><td>"+"<a href='/html/editRide.html'onclick='edit(this.parentNode.parentNode.rowIndex)'>Edit</a>-<a href='#' onclick='eliminar(this.parentNode.parentNode.rowIndex)'>Remove</a>"+"</td></tr>";

		// agregar a la tabla
		var table = document.getElementById("tb_rides");
		table.innerHTML = table.innerHTML + row;
	},

};

function eliminar(j) {
	//Obtener informacion de la fila seleccionada y luego crear un objeto con dicha informacion.
	document.getElementsByTagName("table")[0].setAttribute("id","tableid");

	var rideName = document.getElementById("tableid").rows[j].cells[0].innerHTML;
	var start = document.getElementById("tableid").rows[j].cells[1].innerHTML;
	var end = document.getElementById("tableid").rows[j].cells[2].innerHTML;
	var ride_Delete_Object = {
		rideName,
		start,
		end
	};
	//Leer rides del LS.
	var rides_found = [JSON.parse(localStorage.getItem('rides'))];
	for (var i = rides_found.length - 1; i >= 0; i--) {
		var object = rides_found[i];
		//Se recorre ese objeto para obtener la informacion de el.
		for (var i = object.length - 1; i >= 0; i--) {
			var ob = object[i];
			//Validar que el numero de telefono y el nombre del ride sean iguales para eliminarlo.
			if (ob.phone == phone) {
				if (ob.rideName == ride_Delete_Object.rideName) {
					var elementoEliminado = object.splice(i, 1);
					localStorage["rides"] = JSON.stringify(object);
					document.getElementById("tableid").deleteRow(j);
					alert("Eliminado");
				}
			}
		}
	}
}

//Metodo guarda el ride que se quiere editar,
//para luego ser utilizado y setear los input con dicha informacion.
function edit(j){
	debugger;
	//Obtener informacion de la fila seleccionada y luego crear un objeto con dicha informacion.
	document.getElementsByTagName("table")[0].setAttribute("id","tableid");

	var rideName = document.getElementById("tableid").rows[j].cells[0].innerHTML;
	var start = document.getElementById("tableid").rows[j].cells[1].innerHTML;
	var end = document.getElementById("tableid").rows[j].cells[2].innerHTML;
	var ride_Delete_Object = {
		rideName,
		start,
		end
	};
	//Leer rides del LS.
	var rides_found = [JSON.parse(localStorage.getItem('rides'))];
	for (var i = rides_found.length - 1; i >= 0; i--) {
		var object = rides_found[i];
		//Se recorre ese objeto para obtener la informacion de el.
		for (var i = object.length - 1; i >= 0; i--) {
			var ob = object[i];
			//Validar que el numero de telefono y el nombre del ride sean iguales para editarlo.
			if (ob.phone == phone) {
				if (ob.rideName == ride_Delete_Object.rideName) {
					//Guardar en el localStorage para luego utilizar el ride que se quiere editar.
					localStorage["ridesEdit"] = JSON.stringify(ob);
					var position = i;
					localStorage["position"] = JSON.stringify(position);
				}
			}
		}
	}
}
RIDES.initialize();
