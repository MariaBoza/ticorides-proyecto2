
//Recibe la informacion por parametro del registro del usuario para luego ser utilizada.
Client = function  (name, lastName, phoneNumber, userName, password) {
	this.name = name;
	this.lastName = lastName;
	this.phoneNumber = phoneNumber;
	this.userName = userName;
	this.password = password;

	//Guarda los usuarios.
	this.save = function() {
		//Crear objeto persona.
		var persona = {
			name,
			lastName,
			phoneNumber,
			userName,
			password
		};
		debugger;
		//Se carga todos los datos de todos los usuarios para validar que no hayan repetidos
		//y luego se almacenan en el localStorage.
		var arregloUsers = JSON.parse(localStorage.getItem('Users'));
		if (arregloUsers == null) {
			arregloUsers = [persona];
			localStorage["Users"] = JSON.stringify(arregloUsers);
			alert("REGISTRADO");
		}else{
			arregloUsers.push(persona);
			localStorage["Users"] = JSON.stringify(arregloUsers);
			alert("REGISTRADO");
		}
	}
}

saveClient = function () {
	debugger;
	//Se obtienen los valores de los inputs.
	var name = document.getElementById('name').value;
	var lastName = document.getElementById('lastName').value;
	var phoneNumber = document.getElementById('phone').value;
	var userNameRegister = document.getElementById('userName-register').value;
	var passwordRegister = document.getElementById('password-register').value;
	var repeatPasswordRegister = document.getElementById('repeat-password-register').value;

		//Se valida que la contrasena sea igual
	if (passwordRegister == repeatPasswordRegister) {
			//Se crea una instancia y se envian los datos por parametros.
			var client1 = new Client(name, lastName, phoneNumber, userNameRegister, passwordRegister);
			client1.save(); //Se llama a la function save, que guardara el usuario.
			//Limpia todos los inputs.
			document.getElementById('name').value = "";
			document.getElementById('lastName').value = "";
			document.getElementById('phone').value = "";
			document.getElementById('userName-register').value = "";
			document.getElementById('password-register').value = "";
			document.getElementById('repeat-password-register').value = "";
			document.getElementById('repeat-password-register').focus();
		}else{
			alert("Las contrasenas son diferentes, deben ser iguales");
		}

}
